package frc.robot;
/*********************************************************************************************************
 * CAN SparkMax Closed Loop Velocity Motor Control using NEO
 * Written by Kurt M. Sanger
 * FRC Team 3838
 * Created 2/26/2024
 *
 * Change Class Name to your Motor Name.
 * Adjust the parameters in the MotorParams internal class.
 * Change the ShuffleBoard Tab Name to refer to your Motor Name.
 *
 */
import java.util.concurrent.TimeUnit;

import com.revrobotics.CANSparkBase;
import com.revrobotics.CANSparkBase.ControlType;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import frc.robot.RobotConfig.CANs;
/*
* Change the Class Name to match your Motor Name and Filename.
 */
public class IntakeMotor
{
    /** Adjust Motor Parameters for This Motor
     *
     */
    class MotorParams
    {
        public static String motor = "NEO";
        // PID coefficients
        public static double kP = 6e-6;
        public static double kI = 6e-7;
        public static double kD = 0.0;
        public static double kIz = 0.2;
        public static double kFF = 0.0002;
        public static double kMaxOutput = 1.0;
        public static double kMinOutput = -1.0;
        public static double maxRPM = 5000.0;       // max available is 1/kFF
        // NEO Maximum Unloaded Speed = 5676 RPM
        // NEO 550 Maximum Unloaded Speed = 11000 RPM
        // Set max RPM lower than open loop or maximum loaded speed to enable PID to increase for additional load.
        public static double arbitraryFeedForwardPercent = 0.0;  // use ArbFFUnits.kPercentOut
        public static double startingSetPoint = 2000.0;
        public static int canID = CANs.INTAKE_MOTOR;  // Intake Motor CAN ID = 57
        // Our CAN IDs are in RobotConfig.CANS.
        public static boolean inverted = true;
        public static CANSparkBase.IdleMode idleMode = IdleMode.kBrake;
        public static int smartCurrentLimit = 35;
        public static double secondaryCurrentLimit = 40.0;
        public static long delay = 100;  // 100 msec delay for CAN to init.
        public static double setPoint = 0.0;  // Initialize setpoint to off.
        // TODO Add Current Limits, SmartCurrent and Secondary Current
    }
    
    private CANSparkMax motor;
    private SparkPIDController motorPidController;
    private RelativeEncoder motorEncoder;
    
    /** Set Tab Name for this Motor */
    private ShuffleboardTab motorTab = Shuffleboard.getTab("Intake Motor");
    
    private GenericEntry motorOn = motorTab.add("Motor On", false)
                                            .withSize(1,1)
                                            .withPosition(0,0)
                                            .getEntry();
    private GenericEntry motorSetPoint = motorTab.add("Set Point", MotorParams.startingSetPoint)
                                            .withSize(1,1)
                                            .withPosition(1,0)
                                            .getEntry();
    private GenericEntry motorVelocity = motorTab.add("Velocity (RPM)",  0.0)
                                            .withSize(1,1)
                                             .withPosition(2,0)
                                            .getEntry();
    private GenericEntry motorCurrent = motorTab.add("Current (A)",  0.0)
                                            .withSize(1,1)
                                            .withPosition(3,0)
                                            .getEntry();
    private GenericEntry motorTemperature = motorTab.add("Temperature (C)",  0.0)
                                            .withSize(1,1)
                                            .withPosition(4,0)
                                            .getEntry();
    private GenericEntry motorFF = motorTab.add("FF Gain",  MotorParams.kFF)
                                            .withSize(1,1)
                                            .withPosition(0,4)
                                            .getEntry();
    private GenericEntry motorP = motorTab.add("P Gain",  MotorParams.kP)
                                           .withSize(1,1)
                                           .withPosition(1,4)
                                           .getEntry();
    private GenericEntry motorI = motorTab.add("I Gain",  MotorParams.kI)
                                           .withSize(1,1)
                                           .withPosition(2,4)
                                           .getEntry();
    private GenericEntry motorD = motorTab.add("D Gain",  MotorParams.kD)
                                           .withSize(1,1)
                                           .withPosition(3,4)
                                           .getEntry();
    private GenericEntry motorIz = motorTab.add("I Zone",  MotorParams.kIz)
                                           .withSize(1,1)
                                           .withPosition(4,4)
                                           .getEntry();
    private GenericEntry motorMax = motorTab.add("Max Output",  MotorParams.kMaxOutput)
                                            .withSize(1,1)
                                            .withPosition(5,4)
                                            .getEntry();
    private GenericEntry motorMin = motorTab.add("Min Output",  MotorParams.kMinOutput)
                                            .withSize(1,1)
                                            .withPosition(6,4)
                                            .getEntry();
    private GenericEntry motorArbFFPercent = motorTab.add("Arbitrary FF Percent",  MotorParams.arbitraryFeedForwardPercent)
                                            .withSize(1,1)
                                            .withPosition(7,4)
                                            .getEntry();
            
    
    /**
     *  Change Name to Match Class Name so that it is called during instantiation.
     */
    public  IntakeMotor() throws InterruptedException
    {
        
        motor = new CANSparkMax(MotorParams.canID, MotorType.kBrushless);
        TimeUnit.MILLISECONDS.sleep(MotorParams.delay);
        motor.restoreFactoryDefaults();
        motor.setInverted(MotorParams.inverted);
        motor.setIdleMode(MotorParams.idleMode);
        motor.setSmartCurrentLimit(MotorParams.smartCurrentLimit);
        motor.setSecondaryCurrentLimit(MotorParams.secondaryCurrentLimit);
        stopMotor();   // Turn Motor Off
        TimeUnit.MILLISECONDS.sleep(MotorParams.delay);
        
        // Setup Shuffleboard Tab for this Motor
        motorOn.setBoolean(false);
        motorSetPoint.setDouble(MotorParams.startingSetPoint);
        motorVelocity.setDouble(0.0);
        motorCurrent.setDouble(0.0);
        motorTemperature.setDouble(0.0);
        motorFF.setDouble( MotorParams.kFF);
        motorP.setDouble( MotorParams.kP);
        motorI.setDouble( MotorParams.kI);
        motorD.setDouble( MotorParams.kD);
        motorIz.setDouble( MotorParams.kIz);
        motorMax.setDouble( MotorParams.kMaxOutput);
        motorMin.setDouble( MotorParams.kMinOutput);
        motorArbFFPercent.setDouble( MotorParams.arbitraryFeedForwardPercent);
        
        // Setup PID Velocity Control
        motorPidController = motor.getPIDController();
        motorEncoder = motor.getEncoder();
        
        motorPidController.setFF( motorFF.getDouble(MotorParams.kFF),0);
        motorPidController.setP( motorP.getDouble(MotorParams.kP),0);
        motorPidController.setI( motorI.getDouble(MotorParams.kI),0);
        motorPidController.setD( motorD.getDouble(MotorParams.kD),0);
        motorPidController.setIZone( motorIz.getDouble(MotorParams.kIz),0);
        motorPidController.setOutputRange(motorMin.getDouble(MotorParams.kMinOutput),
                                          motorMax.getDouble(MotorParams.kMaxOutput),0);
    }
    
    public void stopMotor() {
        motor.stopMotor();
        motorOn.setBoolean(false);
    }
    
    public void motorOn() {
        double setPoint = motorSetPoint.getDouble(MotorParams.setPoint);
        double arbFFPercent = motorArbFFPercent.getDouble(MotorParams.arbitraryFeedForwardPercent);
        //motorPidController.setReference(setPoint, CANSparkMax.ControlType.kVelocity);
        motorPidController.setReference(setPoint, ControlType.kVelocity, 0,
                                        arbFFPercent, SparkPIDController.ArbFFUnits.kPercentOut);
        motorOn.setBoolean(true);
    }
    
    public boolean getMotorOn() {
        return motorOn.getBoolean(false);
    }
    
    public double getVelocity() {
        return motorEncoder.getVelocity();
    }
    
    public double getCurrent() {
        return motor.getOutputCurrent();
    }
    
    public double getTemperature() {
        return motor.getMotorTemperature();
    }
    
    public double getSetPoint() {
        return motorSetPoint.getDouble(0.0);
    }
    
    public void setSetPoint( double setPoint)
    {
        if( setPoint <= MotorParams.maxRPM)
        {
            if (setPoint >= -MotorParams.maxRPM)
            {
                motorSetPoint.setDouble(setPoint);
            }
            else
            {
                motorSetPoint.setDouble(-MotorParams.maxRPM);
            }
        } else
        {
            motorSetPoint.setDouble( MotorParams.maxRPM);
        }
    }
    
    public void updateMotor() {
        
        // Check ShuffleBoard for input changes
        // Note need to check that changes are valid.
        double ff = motorFF.getDouble(MotorParams.kFF);
        double p = motorP.getDouble(MotorParams.kP);
        double i = motorI.getDouble(MotorParams.kI);
        double d = motorD.getDouble(MotorParams.kD);
        double iz = motorIz.getDouble(MotorParams.kIz);
        double max = motorMax.getDouble(MotorParams.kMaxOutput);
        double min = motorMin.getDouble(MotorParams.kMinOutput);
        double arbFFPercent = motorArbFFPercent.getDouble(MotorParams.arbitraryFeedForwardPercent);
        double setPoint = motorSetPoint.getDouble(MotorParams.setPoint);
        
        // if PID coefficients on ShuffeBoard have changed, write new values to controller
        if((ff != MotorParams.kFF)) { motorPidController.setFF(ff,0); MotorParams.kFF = ff; }
        if((p != MotorParams.kP)) { motorPidController.setP(p,0); MotorParams.kP = p; }
        if((i != MotorParams.kI)) { motorPidController.setI(i,0); MotorParams.kI = i; }
        if((d != MotorParams.kD)) { motorPidController.setD(d,0); MotorParams.kD = d; }
        if((iz != MotorParams.kIz)) { motorPidController.setIZone(iz,0); MotorParams.kIz = iz; }
        if ((max != MotorParams.kMaxOutput) || (min != MotorParams.kMinOutput)) {
            motorPidController.setOutputRange(min,max,0);
            MotorParams.kMinOutput = min;
            MotorParams.kMaxOutput = max;
        }
        if((arbFFPercent != MotorParams.arbitraryFeedForwardPercent)) {
            MotorParams.arbitraryFeedForwardPercent = arbFFPercent;
            // Set New Arbitrary Feed Forward Speed and Resend Motor Command
            if(getMotorOn()) { motorOn(); }
        }
        if( setPoint != MotorParams.setPoint) {
            MotorParams.setPoint = setPoint;
            // Set New Set Point and Resend Motor Command.
            if( getMotorOn()) { motorOn(); }
        }
        
        // Update Motor Stats Velocity, Current, Temperature.
        motorVelocity.setDouble(motorEncoder.getVelocity());
        motorCurrent.setDouble(motor.getOutputCurrent());
        motorTemperature.setDouble(motor.getMotorTemperature());
    }
}
