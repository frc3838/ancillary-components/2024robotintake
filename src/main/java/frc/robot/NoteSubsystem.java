package frc.robot;
/* **********************************************************************************************
 * Note Subsystem, Intake, Transport, and Shooter Motors using Closed Loop Velocity Speed Control
 * Written by Kurt M. Sanger
 * FRC Team 3838
 * Created 2/26/2024
 *
 * Change Class Name to your Motor Name.
 * Adjust the parameters in the MotorParams internal class.
 * Change the ShuffleBoard Tab Name to refer to your Motor Name.
 *
 */
import java.util.concurrent.TimeUnit;

import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import frc.robot.RobotConfig.DIOs;

public class NoteSubsystem
{
    public class NoteParams {
        private static int  noteSensorDIO = DIOs.TRANSPORT_LIMIT_SWITCH;
        private static boolean activeLow = false;
        public static double intakeSpeedIn = 2000;  // Intake Speed RPM for Intaking
        public static double kickerSpeedIn = 2000;  // Kicker Speed RPM for Intaking
        public static double transportSpeedIn = 3000;  // Transport Speed RPM for Intaking
        public static double transportSpeedSpeaker = 3000; // Transport Speed RPM for Shooting Speaker
        public static double transportSpeedAmp = 3000; // Transport Speed RPM for Shooting Amp
        public static double shooterSpeedSpeaker = 5000; // Transport Speed RPM for shooting at the Speaker
        public static double shooterSpeedAmp = 1000; // Transport Speed RPM for shooting at the Speaker
        public static long delayTime = 500;
        public static double speedTolerance = 100.0;
    }
    
    private boolean noteSubsystemIsReady = false;
    private IntakeMotor intakeMotor;
    private KickerMotor kickerMotor;
    private TransportLeftMotor transportLeftMotor;
    private TransportRightMotor transportRightMotor;
    private ShooterLeftMotor shooterLeftMotor;
    private ShooterRightMotor shooterRightMotor;
    
    private DigitalInput noteSensor;
    
    private ShuffleboardTab noteTab = Shuffleboard.getTab("Note Subsystem");
    private GenericEntry noteIn = noteTab.add("Have Note", false)
            .withSize(1,1)
            .withPosition(0,0)
            .getEntry();
    private GenericEntry intakeRPM = noteTab.add("Intake RPM", 0.0)
            .withSize(1,1)
            .withPosition(2,0)
            .getEntry();
    private GenericEntry kickerRPM = noteTab.add("Kicker RPM", 0.0)
            .withSize(1,1)
            .withPosition(3,0)
            .getEntry();
    private GenericEntry leftTransportRPM = noteTab.add("Left Transport RPM", 0.0)
            .withSize(1,1)
            .withPosition(4,0)
            .getEntry();
    private GenericEntry rightTransportRPM = noteTab.add("Right Transport RPM",0.0)
            .withSize(1,1)
            .withPosition( 5,0)
            .getEntry();
    private GenericEntry leftShooterRPM = noteTab.add("Left Shooter RPM", 0.0)
            .withSize(1,1)
            .withPosition(6,0)
            .getEntry();
    private GenericEntry rightShooterRPM = noteTab.add("Right Shooter RPM", 0.0)
            .withSize(1,1)
            .withPosition(7,0)
            .getEntry();
    private GenericEntry intakeOn = noteTab.add("Intake On", false)
            .withSize(1,1)
            .withPosition(0,1)
            .getEntry();
    private GenericEntry shooterSpeakerOn = noteTab.add("Shooter Speaker On", false)
            .withSize(1,1)
            .withPosition(0,2)
            .getEntry();
    private GenericEntry shooterAmpOn = noteTab.add("Shooter Amp On", false)
            .withSize(1,1)
            .withPosition(0,3)
            .getEntry();
    private GenericEntry intakeIn = noteTab.add("Intake Set Speed RPM", NoteParams.intakeSpeedIn)
                                           .withSize(1,1)
                                           .withPosition(2,1)
                                           .getEntry();
    private GenericEntry kickerIn = noteTab.add("Kicker Set Speed RPM", NoteParams.kickerSpeedIn)
                                           .withSize(1,1)
                                           .withPosition(3,1)
                                           .getEntry();
    private GenericEntry transportIn = noteTab.add("Transport Set Speed In", NoteParams.transportSpeedIn)
                                              .withSize(1,1)
                                              .withPosition(4,1)
                                              .getEntry();
    private GenericEntry transportSpeaker = noteTab.add("Transport Speaker RPM", NoteParams.transportSpeedSpeaker)
                                            .withSize(1,1)
                                            .withPosition(4,2)
                                            .getEntry();
    private GenericEntry shooterSpeaker = noteTab.add("Shooter Speaker RPM", NoteParams.shooterSpeedSpeaker)
                                                   .withSize(1,1)
                                                   .withPosition(5,2)
                                                   .getEntry();
    private GenericEntry transportAmp = noteTab.add("Transport Amp RPM", NoteParams.transportSpeedAmp)
                                                 .withSize(1,1)
                                                 .withPosition(4,3)
                                                 .getEntry();
    private GenericEntry shooterAmp = noteTab.add("Shooter Amp RPM", NoteParams.shooterSpeedAmp)
                                               .withSize(1,1)
                                               .withPosition(5,3)
                                               .getEntry();
    private GenericEntry speedTolerance = noteTab.add("Speed Tolerance", NoteParams.speedTolerance)
            .withSize(1,1)
            .withPosition(1,3)
            .getEntry();
    private GenericEntry leftShooterAtSpeakerSpeed = noteTab.add("Left SpeakerAtSpeed", false)
                                                            .withSize(1,1)
                                                            .withPosition(6,2)
                                                            .getEntry();
    private GenericEntry rightShooterAtSpeakerSpeed = noteTab.add("Right SpeakerAtSpeed", false)
                                                             .withSize(1,1)
                                                             .withPosition(7,2)
                                                             .getEntry();
    private GenericEntry leftShooterAtAmpSpeed = noteTab.add("Left AmpAtSpeed", false)
                                                            .withSize(1,1)
                                                            .withPosition(6,3)
                                                            .getEntry();
    private GenericEntry rightShooterAtAmpSpeed = noteTab.add("Right AmpAtSpeed", false)
                                                             .withSize(1,1)
                                                             .withPosition(7,3)
                                                             .getEntry();
            
    
    public NoteSubsystem() throws InterruptedException
    {
        // Initialize Motors
        intakeMotor = new IntakeMotor();
        kickerMotor = new KickerMotor();
        transportLeftMotor = new TransportLeftMotor();
        transportRightMotor = new TransportRightMotor();
        shooterLeftMotor = new ShooterLeftMotor();
        shooterRightMotor = new ShooterRightMotor();
        // Wait for init to finish.
        TimeUnit.MILLISECONDS.sleep(NoteParams.delayTime);
        
        off();
        // Initialize note sensor.
        noteSensor = new DigitalInput(NoteParams.noteSensorDIO);
        
        noteIn.setBoolean(haveNote());
        //ampGuideUp.setBoolean(false);  // Assume we start with amp guide down.
        intakeRPM.setDouble(0.0);      // Assume we start with all motors off.
        kickerRPM.setDouble(0.0);
        leftTransportRPM.setDouble(0.0);
        rightTransportRPM.setDouble(0.0);
        leftShooterRPM.setDouble(0.0);
        rightShooterRPM.setDouble(0.0);
        intakeIn.setDouble(NoteParams.intakeSpeedIn);  // Set Default Speeds
        kickerIn.setDouble(NoteParams.kickerSpeedIn);
        transportIn.setDouble(NoteParams.transportSpeedIn);
        transportSpeaker.setDouble(NoteParams.transportSpeedSpeaker);
        shooterSpeaker.setDouble(NoteParams.shooterSpeedSpeaker);
        transportAmp.setDouble(NoteParams.transportSpeedAmp);
        shooterAmp.setDouble(NoteParams.shooterSpeedAmp);
        intakeOn.setBoolean(false);
        shooterSpeakerOn.setBoolean(false);
        shooterAmpOn.setBoolean(false);
        speedTolerance.setDouble(NoteParams.speedTolerance);
        leftShooterAtSpeakerSpeed.setBoolean(false);
        rightShooterAtSpeakerSpeed.setBoolean(false);
        leftShooterAtAmpSpeed.setBoolean(false);
        rightShooterAtAmpSpeed.setBoolean(false);
        noteSubsystemIsReady = true;
    }
    
    public boolean isReady() {
        return noteSubsystemIsReady;
    }
    public void off() {
        intakeMotor.stopMotor();
        kickerMotor.stopMotor();
        transportLeftMotor.stopMotor();
        transportRightMotor.stopMotor();
        shooterLeftMotor.stopMotor();
        shooterRightMotor.stopMotor();
        intakeOn.setBoolean(false);
        shooterSpeakerOn.setBoolean(false);
        shooterAmpOn.setBoolean(false);
    }
    
    public boolean haveNote() {
        boolean flgHaveNote = false;
        
        /*  have Note truth table
        noteSensor.get()    activeLow   haveNote()
        0                   0           0
        0                   1           1
        1                   1           0
        1                   0           1
        
        haveNote() = (not(noteSensor.get()) and activeLow) or
                     (noteSensor.get() and not(activeLow))
         */
        
        if(noteSensor.get()) {
            flgHaveNote = !NoteParams.activeLow;
        } else {
            flgHaveNote = NoteParams.activeLow;
        }
        noteIn.setBoolean( flgHaveNote);
        return noteIn.getBoolean(false);
    }
    
    public void intakeOn() {
        intakeMotor.setSetPoint( intakeIn.getDouble(NoteParams.intakeSpeedIn));
        kickerMotor.setSetPoint( kickerIn.getDouble(NoteParams.kickerSpeedIn));
        transportLeftMotor.setSetPoint( transportIn.getDouble(NoteParams.transportSpeedIn));
        transportRightMotor.setSetPoint( transportIn.getDouble( NoteParams.transportSpeedIn));
        
        intakeMotor.motorOn();
        kickerMotor.motorOn();
        transportLeftMotor.motorOn();
        transportRightMotor.motorOn();
        intakeOn.setBoolean(true);
    }
    
    public void intakeOff() {
        transportLeftMotor.stopMotor();
        transportRightMotor.stopMotor();
        intakeMotor.stopMotor();
        kickerMotor.stopMotor();
        intakeOn.setBoolean(false);
    }
    
    public void armShooterForSpeaker()  // Sets Speeds and Turns Shooter Motors On
    {
        shooterLeftMotor.setSetPoint( shooterSpeaker.getDouble(NoteParams.shooterSpeedSpeaker));
        shooterRightMotor.setSetPoint( shooterSpeaker.getDouble(NoteParams.shooterSpeedSpeaker));
        transportLeftMotor.setSetPoint( transportSpeaker.getDouble(NoteParams.transportSpeedSpeaker));
        transportRightMotor.setSetPoint( transportSpeaker.getDouble( NoteParams.transportSpeedSpeaker));
        shooterOn(0);
    }
    
    public void armShooterForAmp()  // Sets Speeds and Turns Shooter Motors On
    {
        shooterLeftMotor.setSetPoint( shooterAmp.getDouble(NoteParams.shooterSpeedAmp));
        shooterRightMotor.setSetPoint( shooterAmp.getDouble(NoteParams.shooterSpeedAmp));
        transportLeftMotor.setSetPoint( transportAmp.getDouble(NoteParams.transportSpeedAmp));
        transportRightMotor.setSetPoint( transportAmp.getDouble( NoteParams.transportSpeedAmp));
        shooterOn(1);
    }
    
    public void shooterOn(int shooterType) {
        shooterLeftMotor.motorOn();
        shooterRightMotor.motorOn();
        if (shooterType==0) {
            shooterSpeakerOn.setBoolean(true);
            shooterAmpOn.setBoolean(false);
        } else if(shooterType==1) {
            shooterSpeakerOn.setBoolean(false);
            shooterAmpOn.setBoolean(true);
        }
    }
    
    public void shoot() {
        if( shooterSpeakerOn.getBoolean(false) || shooterAmpOn.getBoolean(false)) {
            transportLeftMotor.motorOn();
            transportRightMotor.motorOn();
        }
    }
    
    public void shooterOff() {
        transportLeftMotor.stopMotor();
        transportRightMotor.stopMotor();
        shooterLeftMotor.stopMotor();
        shooterRightMotor.stopMotor();
        shooterSpeakerOn.setBoolean(false);
        shooterAmpOn.setBoolean(false);
    }
    
    public boolean shooterMotorsReady() {
        // TODO add at speed within tolerance test
        return (shooterLeftMotor.getMotorOn() && shooterRightMotor.getMotorOn());
    }
    
    public void update() {
        // TODO add error feedback for stall, overCurrent, and overTemperature.
        // Check Input Speed for any changes.
        double inIn = intakeIn.getDouble(NoteParams.intakeSpeedIn);
        double kIn = kickerIn.getDouble(NoteParams.kickerSpeedIn);
        double tIn = transportIn.getDouble(NoteParams.transportSpeedIn);
        double tSS = transportSpeaker.getDouble(NoteParams.transportSpeedSpeaker);
        double sSS = shooterSpeaker.getDouble(NoteParams.shooterSpeedSpeaker);
        double tSA = transportAmp.getDouble(NoteParams.transportSpeedAmp);
        double sSA = shooterAmp.getDouble(NoteParams.shooterSpeedAmp);
        double sT = speedTolerance.getDouble(NoteParams.speedTolerance);
        
        boolean tOn = intakeOn.getBoolean(false);
        if(inIn != NoteParams.intakeSpeedIn)
        {
            NoteParams.intakeSpeedIn = inIn;
            if (tOn) intakeOn();
        }
        if(kIn != NoteParams.kickerSpeedIn)
        {
            NoteParams.kickerSpeedIn = kIn;
            if(tOn) intakeOn();
        }
        if(tIn != NoteParams.transportSpeedIn)
        {
            NoteParams.transportSpeedIn = tIn;
            if(tOn) intakeOn();
        }
        boolean sPOn = shooterSpeakerOn.getBoolean(false);
        if(tSS != NoteParams.transportSpeedSpeaker)
        {
            NoteParams.transportSpeedSpeaker = tSS;
            if(sPOn) armShooterForSpeaker();
        }
        if(sSS != NoteParams.shooterSpeedSpeaker)
        {
            NoteParams.shooterSpeedSpeaker = sSS;
            if(sPOn) armShooterForSpeaker();
        }
        boolean sAOn = shooterAmpOn.getBoolean(false);
        if(tSA != NoteParams.transportSpeedAmp)
        {
            NoteParams.transportSpeedAmp = tSA;
            if(sAOn) armShooterForAmp();
        }
        if(sSA != NoteParams.shooterSpeedAmp)
        {
            NoteParams.shooterSpeedAmp = sSA;
            if(sAOn) armShooterForAmp();
        }
        
        intakeMotor.updateMotor();
        kickerMotor.updateMotor();
        transportLeftMotor.updateMotor();
        transportRightMotor.updateMotor();
        shooterLeftMotor.updateMotor();
        shooterRightMotor.updateMotor();
        
        intakeRPM.setDouble( intakeMotor.getVelocity());
        kickerRPM.setDouble(kickerMotor.getVelocity());
        leftTransportRPM.setDouble(transportLeftMotor.getVelocity());
        rightTransportRPM.setDouble(transportRightMotor.getVelocity());
        double lShSp = shooterLeftMotor.getVelocity();
        leftShooterRPM.setDouble(lShSp);
        double rShSp = shooterRightMotor.getVelocity();
        rightShooterRPM.setDouble(rShSp);
        
        leftShooterAtSpeakerSpeed.setBoolean( ((sSS-sT)<=lShSp) && (lShSp<=(sSS+sT)));
        rightShooterAtSpeakerSpeed.setBoolean( ((sSS-sT)<=rShSp) && (rShSp<=(sSS+sT)));
        leftShooterAtAmpSpeed.setBoolean( ((sSA-sT)<=lShSp) && (lShSp<=(sSA+sT)));
        rightShooterAtAmpSpeed.setBoolean( ((sSA-sT)<=rShSp) && (rShSp<=(sSA+sT)));
        
        
        if( intakeOn.getBoolean(false)) {
            // Check to see if we acquired a note.
            if( haveNote()) {
                intakeOff();
            }
        } else {
            haveNote();
        }
    }
}
