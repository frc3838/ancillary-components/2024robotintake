package frc.robot;
// Copyright (c) FIRST and other WPILib contributors.

// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.
//
// Modified by Kurt M. Sanger 2/26/2024 to use Closed Loop CAN SparkMax Controllers for Velocity and Position.
//

import java.util.concurrent.TimeUnit;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the methods corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */

public class Robot extends TimedRobot
{
    private static final String DEFAULT_AUTO = "Default";
    private static final String CUSTOM_AUTO = "My Auto";
    private String autoSelected;
    private final SendableChooser<String> chooser = new SendableChooser<>();
    
    private Joystick    ourStick;
    private NoteSubsystem noteSubsystem;
    private ClimberSubsystem climbSubsystem;
    private AmpGuideMotor ampGuideMotor;
   
    private ShuffleboardTab robotTab = Shuffleboard.getTab("Robot");
    private GenericEntry ampGuideReady = robotTab.add("Amp Guide Motor Ready", false)
                                            .withSize(1,1)
                                            .withPosition(0,0)
                                            .getEntry();
    private GenericEntry noteSubReady = robotTab.add("Note Subsystem Ready", false)
                                            .withSize(1,1)
                                            .withPosition(1,0)
                                            .getEntry();
    private GenericEntry climbSubReady = robotTab.add("Climber Subsystem Ready", false)
                                            .withSize(1,1)
                                            .withPosition(2,0)
                                            .getEntry();
    
    /**
     * This method is run when the robot is first started up and should be used for any
     * initialization code.
     */
    @Override
    public void robotInit()
    {
        final long timeDelay = 500;
        
        chooser.setDefaultOption("Default Auto", DEFAULT_AUTO);
        chooser.addOption("My Auto", CUSTOM_AUTO);
        SmartDashboard.putData("Auto choices", chooser);
    
        try
        {
            noteSubsystem = new NoteSubsystem();
            TimeUnit.MILLISECONDS.sleep(timeDelay);
        }
        catch (InterruptedException e)
        {
            System.out.println("Run Time Exception: robotInit(): NoteSubsystem.");
            throw new RuntimeException(e);
            
        }
        
        try
        {
            climbSubsystem = new ClimberSubsystem();
            TimeUnit.MILLISECONDS.sleep(timeDelay);
        }
        catch (InterruptedException e)
        {
            System.out.print("Run Time Exception: robotInit(): ClimberSubsystem.");
        }
        
        try
        {
            ampGuideMotor = new AmpGuideMotor();
            TimeUnit.MILLISECONDS.sleep(timeDelay);
        }
        catch (InterruptedException e)
        {
            System.out.println("Run Time Exception: robotInit(): NoteSubsystem.");
            throw new RuntimeException(e);
        }

        if (noteSubsystem.isReady()) {
            noteSubsystem.off();
            noteSubReady.setBoolean(true);
            System.out.println("Note Subsystem Ready.");
        }
        if (climbSubsystem.isReady()) {
            climbSubsystem.stopMotors();
            climbSubReady.setBoolean(true);
            System.out.println("Climb Subsystem Ready.");
        }
        if (ampGuideMotor.isReady()) {
            ampGuideMotor.stopMotor();
            ampGuideReady.setBoolean(true);
            System.out.println("Amp Guide Motor Ready.");
        }
        ourStick = new Joystick(2);
        System.out.println("Robot Init Finished.");
    }
    
    
    /**
     * This method is called every 20 ms, no matter the mode. Use this for items like diagnostics
     * that you want ran during disabled, autonomous, teleoperated and test.
     *
     * <p>This runs after the mode specific periodic methods, but before LiveWindow and
     * SmartDashboard integrated updating.
     */
    @Override
    public void robotPeriodic() {
        updateAll();
    }
    
    
    /**
     * This autonomous (along with the chooser code above) shows how to select between different
     * autonomous modes using the dashboard. The sendable chooser code works with the Java
     * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all the chooser code and
     * uncomment the getString line to get the auto name from the text box below the Gyro
     *
     * <p>You can add additional auto modes by adding additional comparisons to the switch structure
     * below with additional strings. If using the SendableChooser, make sure to add them to the
     * chooser code above as well.
     */
    @Override
    public void autonomousInit()
    {
        autoSelected = chooser.getSelected();
        // autoSelected = SmartDashboard.getString("Auto Selector", DEFAULT_AUTO);
        System.out.println("Auto selected: " + autoSelected);
    }
    
    
    /** This method is called periodically during autonomous. */
    @Override
    public void autonomousPeriodic()
    {
        switch (autoSelected)
        {
            case CUSTOM_AUTO:
                // Put custom auto code here
                break;
            case DEFAULT_AUTO:
            default:
                // Put default auto code here
                break;
        }
    }
    
    
    /** This method is called once when teleop is enabled. */
    @Override
    public void teleopInit() {}
    
    
    /** This method is called periodically during operator control. */
    @Override
    public void teleopPeriodic() {
        
        /************************************************************************************
         *
         *                         Joystick Base Buttons
         *
         *         6  Shoot Speaker Arm                               11  Intake On
         *
         *         7  Shoot Amp Arm                                   10  Intake Off
         *
         *                   8 Shooter Off          9  All Off
         *
         *                   Joystick Trigger = Shoot (Speaker or Amp)
         *
         ************************************************************************************/


        if(noteSubReady.getBoolean(false)) {
            // Intake off / on
            
            if( ourStick.getRawButton(10)){
                noteSubsystem.intakeOff();
            } else if( ourStick.getRawButton(11)) {
                noteSubsystem.intakeOn();
            }
            
            // Shooter on for Speaker, on for Amp, off.
            // Arming Shooter Sets Shooter and Transport Speeds, then Turns Shooter Motors On.
            if( ourStick.getRawButton(6)) {
                noteSubsystem.armShooterForSpeaker();
            } else if( ourStick.getRawButton(7)){
                noteSubsystem.armShooterForAmp();
            } else if( ourStick.getRawButton(8)) {
                noteSubsystem.shooterOff();
            }
            
            // Shooter Fire.   Turns Transport Motors On.
            if( noteSubsystem.shooterMotorsReady() && ourStick.getTrigger()) {
                noteSubsystem.shoot();
            }
            
            // All Off if Systems are Ready
            if( ourStick.getRawButton(9)){
                noteSubsystem.off();
                //climbSubsystem.stopMotors();
                //ampGuideMotor.stopMotor();
            }
        } else if(noteSubsystem.isReady()) {
            noteSubReady.setBoolean(true);
            System.out.println("Note Subsystem Ready Now.");
        }

        if(climbSubReady.getBoolean(false))
        {
            
            /*  *********  Joystick Top Buttons ************************
             *
             *                        3 Extend Climber
             *         4 Amp Guide Up                  5 Amp Guide Down
             *                        2 Retract Climber
             *
             *            Base Btn #9 is used to turn climber off
             ***********************************************************/
            if (ourStick.getRawButton(3))
            {
                // Extend Climber Arms, Descend
                climbSubsystem.extend();
            }
            else if (ourStick.getRawButton(2))
            {
                // Retract Climber Arms, Climb
                climbSubsystem.retract();
            }
            // All Off if Systems are Ready
            if( ourStick.getRawButton(9)){
                //noteSubsystem.off();
                climbSubsystem.stopMotors();
                //ampGuideMotor.stopMotor();
            }
        } else if(climbSubsystem.isReady()) {
            climbSubReady.setBoolean(true);
            System.out.println("Climber Subsystem Ready Now.");
        }
        
        if( ampGuideReady.getBoolean(false)) {
            /* *******************  Joystick Top Buttons ********************
             *
             *                        3 Extend Climber
             *         4 Amp Guide Up                  5 Amp Guide Down
             *                        2 Retract Climber
             *
             *           Base Btn #9 is used to turn Amp Guide Motor Off
             ****************************************************************/
            if( ourStick.getRawButton(4)) {
                // Move Amp Guide Up
                ampGuideMotor.moveUp();
            } else if(ourStick.getRawButton(5)){
                // Move Amp Guide Down
                ampGuideMotor.moveDown();
            }
            // All Off if Systems are Ready
            if( ourStick.getRawButton(9)){
                //noteSubsystem.off();
                //climbSubsystem.stopMotors();
                ampGuideMotor.stopMotor();
            }
        } else if(ampGuideMotor.isReady()) {
            ampGuideReady.setBoolean(true);
            System.out.println("Amp Guide Motor Ready Now.");
        }
    }
    
    public void updateAll() {
        if (ampGuideReady.getBoolean(false))
        {
            ampGuideMotor.updateMotor();  // Update Amp Guide Motor
        }
        
        if (climbSubReady.getBoolean(false))
        {
            climbSubsystem.update();  // Update Climber Subsystem
        }
        
        if (noteSubReady.getBoolean(false))
        {
            noteSubsystem.update();  // Update Note Subsystem
        }
    }
    
    /** This method is called once when the robot is disabled. */
    @Override
    public void disabledInit() {
        if (noteSubReady.getBoolean(false)) { noteSubsystem.off(); };
        if (climbSubReady.getBoolean(false)) { climbSubsystem.stopMotors(); };
        if (ampGuideReady.getBoolean(false)) { ampGuideMotor.stopMotor(); };
    }
    
    
    /** This method is called periodically when disabled. */
    @Override
    public void disabledPeriodic() {
    }
    
    
    /** This method is called once when test mode is enabled. */
    @Override
    public void testInit() {}
    
    
    /** This method is called periodically during test mode. */
    @Override
    public void testPeriodic() {}
    
    
    /** This method is called once when the robot is first started up. */
    @Override
    public void simulationInit() {}
    
    
    /** This method is called periodically whilst in simulation. */
    @Override
    public void simulationPeriodic() {}
}
