package frc.robot;
/*  Robot Config copied CANs and DIOs from FRC 3838 Robot2024
*
 */
public class RobotConfig
{
    public class CANs
    {
        public static int DEFAULT_AND_THEREFORE_RESERVED_ID = 0;
        public static int POWER_DISTRIBUTION_HUB = 1;
        public static int PCM_CAN_ID = 10;
        
        // region SWERVE DRIVE
        public static int SWERVE_FRONT_LEFT__DRIVE_MOTOR =          11;
        public static int SWERVE_FRONT_LEFT__STEER_MOTOR =          12;
        public static int SWERVE_FRONT_LEFT__ABS_STEER_ENCODER =    13;
        public static int SWERVE_FRONT_RIGHT_DRIVE_MOTOR =          21;
        public static int SWERVE_FRONT_RIGHT_STEER_MOTOR =          22;
        public static int SWERVE_FRONT_RIGHT_ABS_STEER_ENCODER =    23;
        public static int SWERVE_REAR__RIGHT_DRIVE_MOTOR =          31;
        public static int SWERVE_REAR__RIGHT_STEER_MOTOR =          32;
        public static int SWERVE_REAR__RIGHT_ABS_STEER_ENCODER =    33;
        public static int SWERVE_REAR__LEFT__DRIVE_MOTOR =          41;
        public static int SWERVE_REAR__LEFT__STEER_MOTOR =          42;
        public static int SWERVE_REAR__LEFT__ABS_STEER_ENCODER =    43;
        // endregion swerve drive
        
        public static int PIGEON_2_GYRO =                           50;
        
        public static int TRANSPORT_MOTOR_LEFT =                    53;
        public static int TRANSPORT_MOTOR_RIGHT =                   54;
        public static int CLIMBER_RIGHT_MOTOR =                     55;
        public static int CLIMBER_LEFT_MOTOR =                      56;
        public static int INTAKE_MOTOR =                            57;
        public static int KICKER_MOTOR =                            58;
        public static int AMP_GUIDE_MOTOR =                         59;
        public static int SHOOTER_LEFT_MOTOR =                      61;
        public static int SHOOTER_RIGHT_MOTOR =                     62;
        
        // @formatter:on
    }

    public class DIOs
    {
        // Please keep assignments in numerical
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT)
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
        
        public static int TRANSPORT_LIMIT_SWITCH = 7;
        public static int CLIMBER_LEFT_RETRACTED_LIMIT_SWITCH = 1;
        public static int CLIMBER_RIGHT_RETRACTED_LIMIT_SWITCH = 0;
        public static int AMP_GUIDE_DOWN_LIMIT_SWITCH = 2;
        public static int AMP_GUIDE_UP_LIMIT_SWITCH = 3;
        public static int CLIMBER_LEFT_EXTENDED_LIMIT_SWITCH = 4;
        public static int CLIMBER_RIGHT_EXTENDED_LIMIT_SWITCH = 5;
    }
    
}
