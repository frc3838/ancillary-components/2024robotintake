package frc.robot;
/* ********************************************************************************************************
 * CAN SparkMax Closed Loop Position Motor Control using NEO
 * Written by Kurt M. Sanger
 * FRC Team 3838
 * Created 2/26/2024
 *
 * Change Class Name to your Motor Name.
 * Adjust the parameters in the MotorParams internal class.
 * Change the ShuffleBoard Tab Name to refer to your Motor Name.
 *
 */
import java.util.concurrent.TimeUnit;
import com.revrobotics.CANSparkBase;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkPIDController;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import frc.robot.RobotConfig.CANs;
import frc.robot.RobotConfig.DIOs;
/*
 * Change the Class Name to match your Motor Name and Filename.
 */
public class ClimberLeftMotor
{
    // TODO add hardware limits up and down.
    
    /** Adjust Motor Parameters for This Motor
     *
     */
    class MotorParams  // For Position Control
    {
        public static String motor = "NEO";
        
        // PID coefficients Initially from Rev Lib Java Closed Loop Position Control Example
        public static double kP = 0.1;          // Position Error (Revolutions) x 0.1
        public static double kI = 1e-4;
        public static double kD = 1;            // Accelerates quickly for a change of 1 revolution.
        public static double kIz = 0.2;
        public static double kFF = 0.0;         // kFF is zero for Position Control
        public static double kMaxOutput = 0.5;  // Reduce kMin and kMax Output to move slower.
        public static double kMinOutput = -0.5; // Reduce kMin and kMax Output to move slower.
        public static double maxRPM = 5000.0;       // max available is 1/kFF
        // TODO add maximum and minimum revolutions.
        // NEO Maximum Unloaded Speed = 5676 RPM
        // NEO 550 Maximum Unloaded Speed = 11000 RPM
        // Set max RPM lower than open loop or maximum loaded speed to enable PID to increase for additional load.
        public static double startingSetPoint = 0.0;
        public static int canID = CANs.CLIMBER_LEFT_MOTOR;  // Left Climber Motor CAN ID = 55 , Right = 56
        public static boolean inverted = false;
        // -115 Revolutions moved from Down to Up.
        /*
            private const val motorClimbSpeed =   0.5   Increasing Revolutions should climb.  Retract Climber Arm
            private const val motorDecentSpeed = -0.4   Decreasing Revolutions should Descend.  Extent Climber Arm
         */
        public static CANSparkBase.IdleMode idleMode = IdleMode.kBrake;
        public static int smartCurrentLimit = 35;
        public static double secondaryCurrentLimit = 40.0;
        public static long delay = 100;  // 100 msec delay for CAN to init.
        public static double setPoint = 0.0;  // Initialize setpoint to off.
    }
    
    CANSparkMax motor;
    private SparkPIDController motorPidController;
    private RelativeEncoder motorEncoder;
    private DigitalInput upLimitSwitch;
    private DigitalInput downLimitSwitch;
    private final static Boolean upLimitSwitchActiveLow = true;
    private final static Boolean downLimitSwitchActiveLow = true;
    
    /** Set Tab Name for this Motor */
    private ShuffleboardTab motorTab = Shuffleboard.getTab("Climber Left Motor");
    
    private GenericEntry motorOn = motorTab.add("Motor On", false)
                                           .withSize(1,1)
                                           .withPosition(0,0)
                                           .getEntry();
    private GenericEntry motorSetPoint = motorTab.add("Set Point", MotorParams.startingSetPoint)
                                                 .withSize(1,1)
                                                 .withPosition(1,0)
                                                 .getEntry();
    private GenericEntry motorPosition = motorTab.add("Position (Revs)",  0.0)
                                                 .withSize(1,1)
                                                 .withPosition(2,0)
                                                 .getEntry();
    private GenericEntry motorCurrent = motorTab.add("Current (A)",  0.0)
                                                .withSize(1,1)
                                                .withPosition(3,0)
                                                .getEntry();
    private GenericEntry motorTemperature = motorTab.add("Temperature (C)",  0.0)
                                                    .withSize(1,1)
                                                    .withPosition(4,0)
                                                    .getEntry();
    private GenericEntry climberUp = motorTab.add("Climber Up",  false)
                                           .withSize(1,1)
                                           .withPosition(5,0)
                                           .getEntry();
    private GenericEntry climberDown = motorTab.add("Climber Down",  false)
                                           .withSize(1,1)
                                           .withPosition(6,0)
                                           .getEntry();

    private GenericEntry motorFF = motorTab.add("FF Gain",  MotorParams.kFF)
                                           .withSize(1,1)
                                           .withPosition(0,4)
                                           .getEntry();
    private GenericEntry motorP = motorTab.add("P Gain",  MotorParams.kP)
                                          .withSize(1,1)
                                          .withPosition(1,4)
                                          .getEntry();
    private GenericEntry motorI = motorTab.add("I Gain",  MotorParams.kI)
                                          .withSize(1,1)
                                          .withPosition(2,4)
                                          .getEntry();
    private GenericEntry motorD = motorTab.add("D Gain",  MotorParams.kD)
                                          .withSize(1,1)
                                          .withPosition(3,4)
                                          .getEntry();
    private GenericEntry motorIz = motorTab.add("I Zone",  MotorParams.kIz)
                                           .withSize(1,1)
                                           .withPosition(4,4)
                                           .getEntry();
    private GenericEntry motorMax = motorTab.add("Max Output",  MotorParams.kMaxOutput)
                                            .withSize(1,1)
                                            .withPosition(5,4)
                                            .getEntry();
    private GenericEntry motorMin = motorTab.add("Min Output",  MotorParams.kMinOutput)
                                            .withSize(1,1)
                                            .withPosition(6,4)
                                            .getEntry();
    private GenericEntry motorConversionFactor = motorTab.add("Motor Conversion Factor", 0.0)
                                            .withSize(1,1)
                                            .withPosition(6,5)
                                            .getEntry();
    
    /**
     *  Change Name to Match Class Name so that it is called during instantiation.
     */
    public  ClimberLeftMotor() throws InterruptedException
    {
        motor = new CANSparkMax(MotorParams.canID, MotorType.kBrushless);
        TimeUnit.MILLISECONDS.sleep(MotorParams.delay);
        motor.restoreFactoryDefaults();
        motor.setInverted(MotorParams.inverted);
        motor.setIdleMode(MotorParams.idleMode);
        motor.setSmartCurrentLimit(MotorParams.smartCurrentLimit);
        motor.setSecondaryCurrentLimit(MotorParams.secondaryCurrentLimit);
        stopMotor();   // Turn Motor Off
        
        motorPidController = motor.getPIDController();
        motorEncoder = motor.getEncoder();
        
        // Setup Shuffleboard Tab for this Motor
        motorOn.setBoolean(false);
        climberUp.setBoolean(false);
        climberDown.setBoolean(false);
        motorSetPoint.setDouble(MotorParams.startingSetPoint);
        motorPosition.setDouble(0.0);
        motorCurrent.setDouble(0.0);
        motorTemperature.setDouble(0.0);
        motorFF.setDouble( MotorParams.kFF);
        motorP.setDouble( MotorParams.kP);
        motorI.setDouble( MotorParams.kI);
        motorD.setDouble( MotorParams.kD);
        motorIz.setDouble( MotorParams.kIz);
        motorMax.setDouble( MotorParams.kMaxOutput);
        motorMin.setDouble( MotorParams.kMinOutput);
        motorConversionFactor.setDouble( motorEncoder.getPositionConversionFactor());
        
        // Setup PID Position Control
        motorPidController.setFF( motorFF.getDouble(MotorParams.kFF));
        motorPidController.setP( motorP.getDouble(MotorParams.kP));
        motorPidController.setI( motorI.getDouble(MotorParams.kI));
        motorPidController.setD( motorD.getDouble(MotorParams.kD));
        motorPidController.setIZone( motorIz.getDouble(MotorParams.kIz));
        motorPidController.setOutputRange(motorMin.getDouble(MotorParams.kMinOutput),
                                          motorMax.getDouble(MotorParams.kMaxOutput));
        
        upLimitSwitch  = new DigitalInput(DIOs.CLIMBER_LEFT_EXTENDED_LIMIT_SWITCH);
        downLimitSwitch = new DigitalInput(DIOs.CLIMBER_LEFT_RETRACTED_LIMIT_SWITCH);
        climberUp.setBoolean(isUp());
        climberDown.setBoolean(isDown());
    }
    
    public boolean isUp() {
        boolean up = false;
        
        if( upLimitSwitch.get())
        {
            up = !upLimitSwitchActiveLow;
        } else {
            up = upLimitSwitchActiveLow;
        }
        climberUp.setBoolean(up);
        return climberUp.getBoolean(false);
        //return upLimitSwitchActiveLow?!upLimitSwitch.get():upLimitSwitch.get();
    }
    public boolean isDown() {
        boolean down = false;
        
        if( downLimitSwitch.get())
        {
            down = !downLimitSwitchActiveLow;
        } else
        {
            down = downLimitSwitchActiveLow;
        }
        climberDown.setBoolean(down);
        return climberDown.getBoolean(false);
        //return downLimitSwitchActiveLow?!downLimitSwitch.get():downLimitSwitch.get();
    }
    
    public void stopMotor() {
        motor.stopMotor();
        motorOn.setBoolean(false);
    }
    
    public void motorOn() {
        double setPoint = motorSetPoint.getDouble(MotorParams.setPoint);
        motorPidController.setReference(setPoint, CANSparkMax.ControlType.kPosition);
        motorOn.setBoolean(true);
    }
    
    public boolean getMotorOn() {
        return motorOn.getBoolean(false);
    }
    
    public double getPosition() {
        return motorEncoder.getPosition();
    }
    
    public double getCurrent() {
        return motor.getOutputCurrent();
    }
    
    public double getTemperature() {
        return motor.getMotorTemperature();
    }
    
    public void resetEncoder() {
        motorEncoder.setPosition(0.0);
    }
    
    public void setPosition(double revolutions){
        motorEncoder.setPosition(revolutions);
    }
    
    public double getSetPoint() {  // Return Revolutions from Start
        return motorSetPoint.getDouble(0.0);
    }
    public void updateMotor() {
        
        // Check ShuffleBoard for input changes
        // Note need to check that changes are valid.
        double ff = motorFF.getDouble(MotorParams.kFF);
        double p = motorP.getDouble(MotorParams.kP);
        double i = motorI.getDouble(MotorParams.kI);
        double d = motorD.getDouble(MotorParams.kD);
        double iz = motorIz.getDouble(MotorParams.kIz);
        double max = motorMax.getDouble(MotorParams.kMaxOutput);
        double min = motorMin.getDouble(MotorParams.kMinOutput);
        double setPoint = motorSetPoint.getDouble(MotorParams.setPoint);
        
        // if PID coefficients on ShuffleBoard have changed, write new values to controller
        if((ff != MotorParams.kFF)) { motorPidController.setFF(ff); MotorParams.kFF = ff; }
        if((p != MotorParams.kP)) { motorPidController.setP(p); MotorParams.kP = p; }
        if((i != MotorParams.kI)) { motorPidController.setI(i); MotorParams.kI = i; }
        if((d != MotorParams.kD)) { motorPidController.setD(d); MotorParams.kD = d; }
        if((iz != MotorParams.kIz)) { motorPidController.setIZone(iz); MotorParams.kIz = iz; }
        if ((max != MotorParams.kMaxOutput) || (min != MotorParams.kMinOutput)) {
            motorPidController.setOutputRange(min,max);
            MotorParams.kMinOutput = min;
            MotorParams.kMaxOutput = max;
        }
        
        if( setPoint != MotorParams.setPoint) {
            MotorParams.setPoint = setPoint;
            if( getMotorOn()) {  // Change setReference to the Controller if the motor is on.
                motorPidController.setReference(setPoint,CANSparkMax.ControlType.kPosition);
            }
        }
        
        // Update Motor Stats Velocity, Current, Temperature.
        motorPosition.setDouble(motorEncoder.getPosition());
        motorCurrent.setDouble(motor.getOutputCurrent());
        motorTemperature.setDouble(motor.getMotorTemperature());
        isUp();
        isDown();
    }
}

