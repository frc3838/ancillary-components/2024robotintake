package frc.robot;
/* ********************************************************************************************************
 * Dual Motor Climber Using Closed Loop Position Control
 * Written by Kurt M. Sanger
 * FRC Team 3838
 * Created 2/26/2024
 *
 * Change Class Name to your Motor Name.
 * Adjust the parameters in the MotorParams internal class.
 * Change the ShuffleBoard Tab Name to refer to your Motor Name.
 *
 * TODO Add Limits
 */
import java.util.concurrent.TimeUnit;

import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;

public class ClimberSubsystem
{
    class ClimberParams
    {
        public static double climberExtendPositionRevs = -115.0;
        public static double climberRetractPositionRevs = 0.0;
    }
    
    private ClimberRightMotor climberRightMotor;
    private ClimberLeftMotor  climberLeftMotor;
    private boolean climberIsReady;
    
    private long timeDelay = 200;  // 200 millisecond delay
    
    private ShuffleboardTab climberTab = Shuffleboard.getTab("Climber Subsystem");
    private GenericEntry extendOn = climberTab.add("Extend CMND Active", false)
            .withSize(1,1)
            .withPosition(0,0)
            .getEntry();
    private GenericEntry retractOn = climberTab.add("Retract CMND Active",false)
            .withSize(1,1)
            .withPosition(1,0)
            .getEntry();
    private GenericEntry leftMotorOn = climberTab.add("Left Motor On", false)
            .withSize(1,1)
            .withPosition(2,0)
            .getEntry();
    private GenericEntry rightMotorOn = climberTab.add("Right Motor On", false)
            .withSize(1,1)
            .withPosition(3,0)
            .getEntry();
    private GenericEntry leftMotorDown = climberTab.add("Left Retracted", false)
            .withSize(1,1)
            .withPosition(2,1)
            .getEntry();
    private GenericEntry rightMotorDown = climberTab.add("Right Retracted", false)
            .withSize(1,1)
            .withPosition(3,1)
            .getEntry();
    private GenericEntry leftMotorUp = climberTab.add("Left Extended", false)
            .withSize(1,1)
            .withPosition(2,2)
            .getEntry();
    private GenericEntry rightMotorUp = climberTab.add("Right Extended", false)
            .withSize(1,1)
            .withPosition(3,2)
            .getEntry();
    private GenericEntry extendedSetPoint = climberTab.add("Extended Set Point (Revs)",
                                                           ClimberParams.climberExtendPositionRevs)
            .withSize(1,1)
            .withPosition(0,3)
            .getEntry();
    private GenericEntry retractedSetPoint = climberTab.add("Retracted Set Point (Revs)",
                                                            ClimberParams.climberRetractPositionRevs)
            .withSize(1,1)
            .withPosition(1,3)
            .getEntry();
    private GenericEntry leftPosition = climberTab.add("Left Position", 0.0)
                                                  .withSize(1,1)
                                                  .withPosition(2,4)
                                                  .getEntry();
    private GenericEntry rightPosition = climberTab.add("Right Position", 0.0)
                                                   .withSize(1,1)
                                                   .withPosition(3,4)
                                                   .getEntry();
    private GenericEntry leftSetPoint = climberTab.add("Left SetPoint", 0.0)
                                                  .withSize(1,1)
                                                  .withPosition(2,3)
                                                  .getEntry();
    private GenericEntry rightSetPoint = climberTab.add("Right SetPoint", 0.0)
                                                   .withSize(1,1)
                                                   .withPosition(3,3)
                                                   .getEntry();
    
    public void ClimberSubsystem() throws InterruptedException
    {
        climberIsReady = false;
        
        climberRightMotor = new ClimberRightMotor();
        TimeUnit.MILLISECONDS.sleep(timeDelay);
        climberLeftMotor = new ClimberLeftMotor();
        TimeUnit.MILLISECONDS.sleep(timeDelay);
        stopMotors();
        // Setup Shuffle Board Tab Sensors and Motor Settings
        extendOn.setBoolean(false);
        retractOn.setBoolean(false);
        leftMotorOn.setBoolean(climberLeftMotor.getMotorOn());
        rightMotorOn.setBoolean(climberRightMotor.getMotorOn());
        leftMotorDown.setBoolean(climberLeftMotor.isDown());
        rightMotorDown.setBoolean(climberRightMotor.isDown());
        leftMotorUp.setBoolean(climberLeftMotor.isUp());
        rightMotorUp.setBoolean(climberRightMotor.isUp());
        extendedSetPoint.setDouble(ClimberParams.climberExtendPositionRevs);
        retractedSetPoint.setDouble(ClimberParams.climberRetractPositionRevs);
        leftPosition.setDouble(climberLeftMotor.getPosition());
        rightPosition.setDouble(climberRightMotor.getPosition());
        leftSetPoint.setDouble(climberLeftMotor.getSetPoint());
        rightSetPoint.setDouble(climberRightMotor.getSetPoint());
        
        climberIsReady = true;
    }
    
    public boolean isReady() {
        return climberIsReady;
    }
    public void setPositionSetpoint(double position)
    {
        climberRightMotor.setPosition( position);
        climberLeftMotor.setPosition(( position));
        leftSetPoint.setDouble(climberLeftMotor.getSetPoint());
        rightSetPoint.setDouble(climberRightMotor.getSetPoint());
    }
    
    public void motorsOn()
    {
        climberRightMotor.motorOn();
        climberLeftMotor.motorOn();
        
        rightMotorOn.setBoolean( climberRightMotor.getMotorOn());
        leftMotorOn.setBoolean( climberLeftMotor.getMotorOn());
    }
    
    public void stopMotors()
    {
        climberRightMotor.stopMotor();
        climberLeftMotor.stopMotor();
        
        rightMotorOn.setBoolean( climberRightMotor.getMotorOn());
        leftMotorOn.setBoolean( climberLeftMotor.getMotorOn());
        retractOn.setBoolean(false);
        extendOn.setBoolean(false);
    }
    
    public void retract()
    {
        setPositionSetpoint(retractedSetPoint.getDouble(ClimberParams.climberRetractPositionRevs));
        motorsOn();
        extendOn.setBoolean(false);
        retractOn.setBoolean(true);
    }
    
    public void extend()
    {
        setPositionSetpoint( extendedSetPoint.getDouble(ClimberParams.climberExtendPositionRevs));
        motorsOn();
        retractOn.setBoolean(false);
        extendOn.setBoolean(true);
        
    }
    
    public double getLeft()
    {
        double position = climberLeftMotor.getPosition();
        leftPosition.setDouble(position);
        return position;
    }
    
    public double getRight()
    {
        double position = climberRightMotor.getPosition();
        rightPosition.setDouble(position);
        return position;
    }
    
    public boolean getLeftRetracted(){
        boolean retracted = climberLeftMotor.isDown();
        leftMotorDown.setBoolean(retracted);
        return retracted;
    }
    
    public boolean getLeftExtended() {
        boolean extended = climberLeftMotor.isUp();
        leftMotorUp.setBoolean(extended);
        return extended;
    }
    
    public boolean getRightRetracted(){
        boolean retracted = climberRightMotor.isDown();
        rightMotorDown.setBoolean(retracted);
        return retracted;
    }
    
    public boolean getRightExtended() {
        boolean extended = climberRightMotor.isUp();
        rightMotorUp.setBoolean(extended);
        return extended;
    }
    
    public double getLeftSetPoint() {
        double setpoint = climberLeftMotor.getSetPoint();
        leftSetPoint.setDouble(setpoint);
        return setpoint;
    }
    
    public double getRightSetPoint() {
        double setpoint = climberRightMotor.getSetPoint();
        rightSetPoint.setDouble(setpoint);
        return setpoint;
    }
    
    public double getExtendedSetPoint() {
        return extendedSetPoint.getDouble(0.0);
    }
    
    public double getRetractedSetPoint() {
        return retractedSetPoint.getDouble(0.0);
    }
    
    public void setExtendedSetPoint( double setPoint)
    {
        extendedSetPoint.setDouble(setPoint);
    }
    
    public void setRetractedSetPoint( double setPoint)
    {
        retractedSetPoint.setDouble( setPoint);
    }
    
    public void updateShuffleBrd() {
        leftMotorOn.setBoolean(climberLeftMotor.getMotorOn());
        rightMotorOn.setBoolean(climberRightMotor.getMotorOn());
        leftMotorDown.setBoolean(climberLeftMotor.isDown());
        rightMotorDown.setBoolean(climberRightMotor.isDown());
        leftMotorUp.setBoolean(climberLeftMotor.isUp());
        rightMotorUp.setBoolean(climberRightMotor.isUp());
        leftPosition.setDouble(climberLeftMotor.getPosition());
        rightPosition.setDouble(climberRightMotor.getPosition());
        leftSetPoint.setDouble(climberLeftMotor.getSetPoint());
        rightSetPoint.setDouble(climberRightMotor.getSetPoint());
    }
    public void update()
    {
        double retractSP = retractedSetPoint.getDouble(ClimberParams.climberRetractPositionRevs);
        double extendSP = extendedSetPoint.getDouble(ClimberParams.climberExtendPositionRevs);
        
        if(retractSP != ClimberParams.climberRetractPositionRevs) {
            ClimberParams.climberRetractPositionRevs = retractSP;
            if(retractOn.getBoolean(false)) {
                setPositionSetpoint(retractSP);
            }
        }
        if(extendSP != ClimberParams.climberExtendPositionRevs) {
            ClimberParams.climberExtendPositionRevs = extendSP;
            if(extendOn.getBoolean(false)) {
                setPositionSetpoint(extendSP);
            }
        }
        
        climberRightMotor.updateMotor();
        climberLeftMotor.updateMotor();
        //updateShuffleBrd
        leftMotorOn.setBoolean(climberLeftMotor.getMotorOn());
        rightMotorOn.setBoolean(climberRightMotor.getMotorOn());
        leftMotorDown.setBoolean(climberLeftMotor.isDown());
        rightMotorDown.setBoolean(climberRightMotor.isDown());
        leftMotorUp.setBoolean(climberLeftMotor.isUp());
        rightMotorUp.setBoolean(climberRightMotor.isUp());
        leftPosition.setDouble(climberLeftMotor.getPosition());
        rightPosition.setDouble(climberRightMotor.getPosition());
        leftSetPoint.setDouble(climberLeftMotor.getSetPoint());
        rightSetPoint.setDouble(climberRightMotor.getSetPoint());
    }
}
